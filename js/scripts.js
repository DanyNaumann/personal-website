//slideshow style interval
var autoSwap = setInterval(swap, 3500);

//pause slideshow and reinstantiate on mouseout
$("ul, span").hover(
    function () {
        clearInterval(autoSwap);
    },
    function () {
        autoSwap = setInterval(swap, 3500);
    }
);

//global variables
var items = [];
var startItem = 1;
var position = 0;
var itemCount = $(".carousel li.items").length;
var leftpos = itemCount;
var resetCount = itemCount;

//unused: gather text inside items class
$("li.items").each(function (index) {
    items[index] = $(this).text();
});

//swap images function
function swap(action) {
    var direction = action;

    //moving carousel backwards
    if (direction == "counter-clockwise") {
        var leftitem = $(".left-pos").attr("id") - 1;
        if (leftitem == 0) {
            leftitem = itemCount;
        }

        $(".right-pos").removeClass("right-pos").addClass("back-pos");
        $(".main-pos").removeClass("main-pos").addClass("right-pos");
        $(".left-pos").removeClass("left-pos").addClass("main-pos");
        $("#" + leftitem + "")
            .removeClass("back-pos")
            .addClass("left-pos");

        startItem--;
        if (startItem < 1) {
            startItem = itemCount;
        }
    }

    //moving carousel forward
    if (direction == "clockwise" || direction == "" || direction == null) {
        function pos(positionvalue) {
            if (positionvalue != "leftposition") {
                //increment image list id
                position++;

                //if final result is greater than image count, reset position.
                if (startItem + position > resetCount) {
                    position = 1 - startItem;
                }
            }

            //setting the left positioned item
            if (positionvalue == "leftposition") {
                //left positioned image should always be one left than main positioned image.
                position = startItem - 1;

                //reset last image in list to left position if first image is in main position
                if (position < 1) {
                    position = itemCount;
                }
            }

            return position;
        }

        $("#" + startItem + "")
            .removeClass("main-pos")
            .addClass("left-pos");
        $("#" + (startItem + pos()) + "")
            .removeClass("right-pos")
            .addClass("main-pos");
        $("#" + (startItem + pos()) + "")
            .removeClass("back-pos")
            .addClass("right-pos");
        $("#" + pos("leftposition") + "")
            .removeClass("left-pos")
            .addClass("back-pos");

        startItem++;
        position = 0;
        if (startItem > itemCount) {
            startItem = 1;
        }
    }
}

//next button click function
$("#next").click(function () {
    swap("clockwise");
});

//prev button click function
$("#prev").click(function () {
    swap("counter-clockwise");
});

//if any visible items are clicked
$("li").click(function () {
    if ($(this).attr("class") == "items left-pos") {
        swap("counter-clockwise");
    } else {
        swap("clockwise");
    }
});

$(window).resize(function () {
    let width = $(".items").width();

    $("#carousel").height(width * 0.66 + "px");

    for (let i = 1; i < 8; i++) {
        $("#" + i).height(width * 0.562 + "px");
    }
});

$(function () {
    $(function () {
        $(window).scroll(function () {
            //Fonction appelée quand on descend la page
            if ($(this).scrollTop() > 200) {
                // Quand on est à 200pixels du haut de page,
                $("#scrollUp").css("right", "10px"); // Replace à 10pixels de la droite l'image
            } else {
                $("#scrollUp").removeAttr("style"); // Enlève les attributs CSS affectés par javascript
            }
        });
    });
});

function darkmode() {
    if ($("#darkModeImg").attr("src") == "images/dark-mode.png") {
        $("#darkModeImg").attr("src", "images/light-mode.png");
        $("#darkMode").css("background-color", "#30b5e6");

        $("#body").removeClass("darkModeBody");
        $("#stats").removeClass("darkModeSection");
        $("#contact").removeClass("darkModeSection");
        $(".cardSkill").removeClass("darkModeSection");
        $(".navbar").removeClass("darkModeNavbar");
        $("h1").removeClass("darkModeText");
        $("h2").removeClass("darkModeText");
        $("p").removeClass("darkModeText");
        $("span").removeClass("darkModeText");
        $("i").removeClass("darkModeText");
        $("#footer").removeClass("darkModeFooter");
        $(".labelForm").removeClass("darkModeText");
    } else {
        $("#darkModeImg").attr("src", "images/dark-mode.png");
        $("#darkMode").css("background-color", "#444");

        $("#body").addClass("darkModeBody");
        $("#stats").addClass("darkModeSection");
        $("#contact").addClass("darkModeSection");
        $(".cardSkill").addClass("darkModeSection");
        $("h1").addClass("darkModeText");
        $("h2").addClass("darkModeText");
        $("p").addClass("darkModeText");
        $("span").addClass("darkModeText");
        $("i").addClass("darkModeText");
        $("#mainH1").removeClass("darkModeText");
        $("#mainSpan").removeClass("darkModeText");
        $(".textPortfolio").removeClass("darkModeText");
        $("#footer").addClass("darkModeFooter");
        $(".labelForm").addClass("darkModeText");

        if ($(window).scrollTop() > $(".navbar").height()) {
            $(".navbar").addClass("darkModeNavbar");
        }
    }
}
